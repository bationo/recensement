<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Communal;
use App\Entity\Departement;
use App\Entity\Militant;
use App\Entity\Prefecture;
use App\Entity\Region;
use App\Entity\Secteur;
use App\Entity\Vote;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MilitantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numeroCarte')
            ->add('adhesion')
            ->add('cni')
            ->add('tel')
            ->add('carte')
            ->add('nom')
            ->add('prenom')
            ->add('lieu')
            ->add('domicile')
            ->add('profession')
            ->add('pere')
            ->add('mere')
            ->add('naissance', DateType::class, array(
                'widget' => 'choice',
                'years' => range(1900,2018)
            ))
            ->add('sexe', ChoiceType::class, array(
                'choices'  => array(
                    'MASCULIN' => 'MASCULIN',
                    'FEMININ' => 'FEMININ'
                ),
            ))

            ->add('disctrict', EntityType::class, array(
                'class' => Category::class,
                'choice_label' => 'libelle',
                'multiple' => false,
            ))
            ->add('region', EntityType::class, array(
                'class' => Region::class,
                'choice_label' => 'libelle',
                'multiple' => false,
            ))
            ->add('departement', EntityType::class, array(
                'class' => Departement::class,
                'choice_label' => 'libelle',
                'multiple' => false,
            ))
            ->add('prefecture', EntityType::class, array(
                'class' => Prefecture::class,
                'choice_label' => 'libelle',
                'multiple' => false,
            ))
            ->add('communal', EntityType::class, array(
                'class' => Communal::class,
                'choice_label' => 'libelle',
                'multiple' => false,
            ))
            ->add('secteur', EntityType::class, array(
                'class' => Secteur::class,
                'choice_label' => 'libelle',
                'multiple' => false,
            ))
            ->add('vote', EntityType::class, array(
                'class' => Vote::class,
                'choice_label' => 'libelle',
                'multiple' => false,
            ))

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Militant::class,
        ]);
    }
}
