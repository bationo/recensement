<?php

namespace App\Form;

use App\Entity\Produit;
use App\Entity\Vendeur;
use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ProduitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('detail')
            ->add('standar')
            ->add('promo')
            ->add('description')
            ->add('caracteristique')
            ->add('modalite')
            ->add('image' , ImageType::class, array( 'required' => false ))
            ->add('vendeur', EntityType::class, array(
                'class' => Vendeur::class,
                'choice_label' => 'nom',
            ))
                ->add('category', EntityType::class, array(
                    'class' => Category::class,
                    'choice_label' => 'libelle',
                    'multiple' => true,
                ))
            ->add('images', CollectionType::class, array( 

             'entry_type'   => ImageType::class,
             'allow_add' => true,
             'by_reference' => false,
             'allow_delete' => true,

            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Produit::class,
        ]);
    }
}
