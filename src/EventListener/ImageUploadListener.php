<?php 
// src/EventListener/ImageUploadListener.php
namespace App\EventListener;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use App\Entity\Image;
use App\Service\FileUploader;

class ImageUploadListener
{
    private $uploader;

    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        
        $entity = $args->getEntity();
        if (!$entity instanceof Image) {
            return;
        }

        $this->preUploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof Image) {
            return;
        }

        $this->preUploadFile($entity);
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Image) {
            return;
        }

        $entity->setFile($this->uploader->getTargetDirectory().'/'.$entity->getId().'.'.$entity->getUrl());
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->postUploadFile($entity);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->postUploadFile($entity);
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Image) {
            return;
        }

        if (file_exists($entity->getFile())) {
            unlink($entity->getFile());
        }
    }

    private function preUploadFile($entity)
    {
        if (!$entity instanceof Image) {
            return;
        }

        $file = $entity->getFile();

        if (!$file instanceof UploadedFile) {
            return;
        }

        if (null === $file) {
            return;
        }

        $entity->setUrl($file->guessExtension());
        $entity->setAlt($file->getClientOriginalName());
    }

    private function postUploadFile($entity)
    {
        // upload only works for Image entities
        if (!$entity instanceof Image) {
            return;
        }

        $file = $entity->getFile();

        // only upload new files
        if (!$file instanceof UploadedFile) {
            return;
        }

        if (null === $file) {
            return;
        }

        if (null !== $entity->getFile()) {
            $oldFile = $this->uploader->getTargetDirectory().'/'.$entity->getId().'.'.$entity->getFile();

            if(file_exists($oldFile)) {
                unlink($oldFile);
            }
        }

        $fileName = $entity->getId().'.'.$entity->getUrl();

        $this->uploader->upload($file, $fileName);
    }
}