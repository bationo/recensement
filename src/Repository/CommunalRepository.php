<?php

namespace App\Repository;

use App\Entity\Communal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Communal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Communal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Communal[]    findAll()
 * @method Communal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommunalRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Communal::class);
    }

//    /**
//     * @return Communal[] Returns an array of Communal objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Communal
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
