<?php

namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 *  @UniqueEntity("email")
 *  @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *   @Assert\Email(
     *     message = "l'adresse '{{ value }}' est invalide.",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Groupes", inversedBy="users")
     * @ORM\JoinTable(name="users_groups",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groupes;


    /**
     * @ORM\Column(type="string", length=255)
     *   @Assert\Length(
     *      min = 6,
     *      minMessage = "le mot de passe doit être formé d'au moins {{ limit }} caractères ",
     * )
     */
    private $password;

    /**
     *  @Assert\EqualTo(propertyPath="password", message="les mots de passe ne correspondent pas" )
     *  
     */
    public $password_confirm;

    public $password_edit;
    
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Admin", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $admin;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Pays" , cascade={"persist"} )
    */
    private $pays;
    
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     * @Assert\Valid
     */
    private $avatar;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Vendeur", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $vendeur;

    /**
     * @ORM\Column(type="datetime")
     *  @Gedmo\Timestampable(on="create")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $modified;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
        $this->groupes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->email;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
    public function getRoles()
    {
        $role = [];
        foreach ($this->getGroupes() as  $value) {
            $role[] = $value->getRole();
        }

        return $role;
    }

    public function getSalt()
    {
    }


    public function eraseCredentials()
    {
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getModified(): ?\DateTimeInterface
    {
        return $this->modified;
    }

    public function setModified(\DateTimeInterface $modified): self
    {
        $this->modified = $modified;

        return $this;
    }
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if($this->id != null){
            $this->modified = new \DateTime();
        }else {
            $this->created = new \DateTime();
            $this->modified = new \DateTime();
        }
        
    }

    public function getAvatar(): ?Image
    {
        return $this->avatar;
    }

    public function setAvatar(?Image $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getAdmin(): ?Admin
    {
        return $this->admin;
    }

    public function setAdmin(?Admin $admin): self
    {
        $this->admin = $admin;

        return $this;
    }
    public function getUploadDir()
    {
        return 'uploads';
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../public/'.$this->getUploadDir();
    }

    public function getWebPath()
    {
        if( $this->getAvatar() != null  ){

            $absolutePath = $this->getUploadDir().'/'.$this->getAvatar()->getId().'.'.$this->getAvatar()->getUrl();

            if(file_exists($absolutePath)) {
                return $absolutePath;
            }else{
                return $this->getUploadDir().'/avatar.png';
            }

        }else{
            return $this->getUploadDir().'/avatar.png';
        }
    

        
    }

    /**
     * @return Collection|Groupes[]
     */
    public function getGroupes(): Collection
    {
        return $this->groupes;
    }

    public function addGroupe(Groupes $groupe): self
    {
        if (!$this->groupes->contains($groupe)) {
            $this->groupes[] = $groupe;
        }

        return $this;
    }

    public function removeGroupe(Groupes $groupe): self
    {
        if ($this->groupes->contains($groupe)) {
            $this->groupes->removeElement($groupe);
        }

        return $this;
    }

    public function getIdentifiant()
    {
        return $this->encrypt('encrypt',$this->id);
    }

    function encrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'ffgd;jhfserfy@!$DCGGgssd';
        $secret_iv = 'ffgd;jhfserfy@@!$DCGGgssd';
        $key = hash('sha256', $secret_key);
        
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    public function getPays(): ?Pays
    {
        return $this->pays;
    }

    public function setPays(?Pays $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getVendeur(): ?Vendeur
    {
        return $this->vendeur;
    }

    public function setVendeur(?Vendeur $vendeur): self
    {
        $this->vendeur = $vendeur;

        return $this;
    } 
    
}
