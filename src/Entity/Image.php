<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 */
class Image
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $url;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $alt;

    /**
     * @Assert\Image()
     */
    private $file;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }


    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getAlt(): ?string
    {
        return $this->alt;
    }

    public function setAlt(string $alt): self
    {
        $this->alt = $alt;

        return $this;
    }
    public function getUploadDir()
    {
        return 'uploads';
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../public/'.$this->getUploadDir();
    }

    public function getWebPath()
    {

            $absolutePath = $this->getUploadDir().'/'.$this->getId().'.'.$this->getUrl();
            if(file_exists($absolutePath)) {
                return $absolutePath;
            }else{
                return $this->getUploadDir().'/no-image.png';
            }
           

    }

}
