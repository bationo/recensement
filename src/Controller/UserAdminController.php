<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Form\PasswordEditType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
* @Route("/admin/user")
*/
class UserAdminController extends AbstractController
{
    /**
     * @Route("/", name="user_admin")
     */
    public function index( ObjectManager $em)
    {
        $entities = $em->getRepository(User::class)->findAdmin();
        

        return $this->render('user_admin/index.html.twig', [
            'entities' => $entities
        ]);
    }

    /**
     * @Route("/login", name="user_admin_login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
    $error = $authenticationUtils->getLastAuthenticationError();

    $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('user_admin/login.html.twig', [
        'last_username' => $lastUsername,
        'error'         => $error,
        ]);
    }



    /**
     * @Route("/remove/{id}", name="user_admin_remove")
     */
    public function remove(Request $request,User $entity,ObjectManager $em)
    {
        $em->remove($entity);
        $em->flush();
        $request->getSession()->getFlashBag()->add('ajout', 'L\'élément a bien été supprimé!'); 
        return $this->redirect($this->generateUrl('user_admin'));
       
    }

    /**
     * @Route("/show/{id}", name="user_admin_show")
     */
    public function show(User $entity,ObjectManager $em)
    {
        return $this->render('user_admin/show.html.twig', [
            'entity' => $entity
        ]);
    }

    /**
     * @Route("/change-password", name="user_admin_password_change")
     */
    public function passwordCurrentUser(Request $request,UserPasswordEncoderInterface $encoder,ObjectManager $em)
    {
        $entity = $this->getUser();

        $form = $this->createForm( PasswordEditType::class, $entity  );

        if ($form->handleRequest($request)->isSubmitted() &&  $form->isValid() ) {
          
            $bool = ($encoder->isPasswordValid($this->getUser() , $request->get('password_edit')['current_password'])) ? true : false;
            if($bool){
                $hash = $encoder->encodePassword($entity, $request->get('password_edit')['password_edit']['first'] );
                $entity->setPassword($hash);
                $em->flush();
                $request->getSession()->getFlashBag()->add('ajout', 'Opération réussie avec succès !'); 
            }else{
              
                $request->getSession()->getFlashBag()->add('error', 'Votre  mot de passe de actuel est incorect !'); 
            }

          
        }

        return $this->render('user_admin/passwordEdit.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }


    /**
     * @Route("/password-edit/{id}", name="user_admin_password")
     */
    public function password(Request $request,User $entity,UserPasswordEncoderInterface $encoder,ObjectManager $em)
    {

        $form = $this->createForm( PasswordEditType::class, $entity  );
        $currentUser = false;
        $currentPassword = null;

        if ($form->handleRequest($request)->isSubmitted() &&  $form->isValid() ) {
          
            $bool = ($encoder->isPasswordValid($this->getUser() , $request->get('password_edit')['current_password'])) ? true : false;
            if($bool){
                $hash = $encoder->encodePassword($entity, $request->get('password_edit')['password_edit']['first'] );
                $entity->setPassword($hash);
                $em->flush();
                $request->getSession()->getFlashBag()->add('ajout', 'Opération réussie avec succès !'); 
            }else{
              
                $request->getSession()->getFlashBag()->add('error', 'Le mot de passe de l\'utilisateur est incorect !'); 
            }

          
        }

        return $this->render('user_admin/password.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * @Route("/create/{id}", name="user_admin_create")
     */
    public function create(Request $request, UserPasswordEncoderInterface $encoder, ObjectManager $em, $id = null )
    {
        $user = new User();
        $etat = false;
        if( $id != null  ){
            $user = $em->getRepository(User::class)->find($id);
        }  
        if($user->getId() != null  )  $etat = true;
        $form = $this->createForm( UserType::class, $user , [ 'admin' => true , 'edit' => $etat ] );
        if ($form->handleRequest($request)->isSubmitted() &&  $form->isValid() ) {
            //dump($user->getAvatar()->getFile()); die();
            $hash = $encoder->encodePassword($user, $user->getPassword() );
            $user->setPassword($hash);
            $em->persist($user);
            $em->flush();
            $request->getSession()->getFlashBag()->add('ajout', 'Opération réussie avec succès !'); 
            return $this->redirect($this->generateUrl('user_admin_show', array('id' => $user->getIdentifiant())));

        
        }

        return $this->render('user_admin/new.html.twig', [
            'form'   => $form->createView(),
            'user' => $user
        ]);
    }

}
