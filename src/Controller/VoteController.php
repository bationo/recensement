<?php

namespace App\Controller;

use App\Entity\Vote;
use App\Form\VoteType;
use App\Repository\VoteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/vote")
 */
class VoteController extends AbstractController
{
    /**
     * @Route("/", name="vote_index", methods="GET")
     */
    public function index(VoteRepository $voteRepository): Response
    {
        return $this->render('vote/index.html.twig', ['votes' => $voteRepository->findAll()]);
    }

    /**
     * @Route("/new/{id}", name="vote_new", methods="GET|POST")
     */
    public function new(Request $request, $id = null): Response
    {
        $vote = new Vote();
        $em = $this->getDoctrine()->getManager();

        if($id != null ){
            $vote = $em->getRepository(Vote::class)->find($id);
        }
        $form = $this->createForm(VoteType::class, $vote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($vote);
            $em->flush();

            return $this->redirectToRoute('vote_index');
        }

        return $this->render('vote/new.html.twig', [
            'vote' => $vote,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="vote_show", methods="GET")
     */
    public function show(Vote $vote): Response
    {
        return $this->render('vote/show.html.twig', ['vote' => $vote]);
    }

    /**
     * @Route("/{id}/edit", name="vote_edit", methods="GET|POST")
     */
    public function edit(Request $request, Vote $vote): Response
    {
        $form = $this->createForm(VoteType::class, $vote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('vote_edit', ['id' => $vote->getId()]);
        }

        return $this->render('vote/edit.html.twig', [
            'vote' => $vote,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="vote_delete", methods="DELETE")
     */
    public function delete(Request $request, Vote $vote): Response
    {
        if ($this->isCsrfTokenValid('delete'.$vote->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($vote);
            $em->flush();
        }

        return $this->redirectToRoute('vote_index');
    }
}
