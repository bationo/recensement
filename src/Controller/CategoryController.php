<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/category")
 */
class CategoryController extends Controller
{
    /**
     * @Route("/", name="category_index", methods="GET")
     */
    public function index(CategoryRepository $categoryRepository): Response
    {
        return $this->render('category/index.html.twig', ['categories' => $categoryRepository->findAll()]);
    }

    /**
     * @Route("/new/{id}", name="category_new", methods="GET|POST")
     */
    public function new(Request $request, $id = null ): Response
    {
        $category = new Category();
        $em = $this->getDoctrine()->getManager();
        if($id != null ){
            $query = $this->encrypt('decrypt', $id);
            $category = $em->getRepository(Category::class)->find($query);
        }
       

        
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $em->persist($category);
            $em->flush();
            $request->getSession()->getFlashBag()->add('ajout', 'Opération réussie avec succès !');
            return $this->redirectToRoute('category_show' , [ 'id' => $category->getIdentifiant()  ] );
        }

        return $this->render('category/new.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="category_show", methods="GET")
     */
    public function show(Category $category): Response
    {

        return $this->render('category/show.html.twig', ['category' => $category]);
    }



    /**
     * @Route("/{id}", name="category_delete", methods="DELETE")
     */
    public function delete(Request $request, Category $category): Response
    {
        $em = $this->getDoctrine()->getManager();
        if ($this->isCsrfTokenValid('delete'.$category->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
        }
        $request->getSession()->getFlashBag()->add('ajout', 'L\'élément a bien été supprimé!'); 
        return $this->redirectToRoute('category_index');
    }

}
