<?php

namespace App\Controller;

use App\Entity\Militant;
use App\Form\MilitantType;
use App\Repository\MilitantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/militant")
 */
class MilitantController extends AbstractController
{
    /**
     * @Route("/", name="militant_index", methods="GET")
     */
    public function index(MilitantRepository $militantRepository): Response
    {
        return $this->render('militant/index.html.twig', ['militants' => $militantRepository->findAll()]);
    }

    /**
     * @Route("/new/{id}", name="militant_new", methods="GET|POST")
     */
    public function new(Request $request, $id= null): Response
    {
        $militant = new Militant();
        $em = $this->getDoctrine()->getManager();

        if($id != null ){
            $militant = $em->getRepository(Militant::class)->find($id);
        }
        $form = $this->createForm(MilitantType::class, $militant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($militant);
            $em->flush();

            return $this->redirectToRoute('militant_index');
        }

        return $this->render('militant/new.html.twig', [
            'militant' => $militant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="militant_show", methods="GET")
     */
    public function show(Militant $militant): Response
    {
        return $this->render('militant/show.html.twig', ['militant' => $militant]);
    }

    /**
     * @Route("/{id}/edit", name="militant_edit", methods="GET|POST")
     */
    public function edit(Request $request, Militant $militant): Response
    {
        $form = $this->createForm(MilitantType::class, $militant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('militant_edit', ['id' => $militant->getId()]);
        }

        return $this->render('militant/edit.html.twig', [
            'militant' => $militant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="militant_delete", methods="DELETE")
     */
    public function delete(Request $request, Militant $militant): Response
    {
        if ($this->isCsrfTokenValid('delete'.$militant->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($militant);
            $em->flush();
        }

        return $this->redirectToRoute('militant_index');
    }
}
