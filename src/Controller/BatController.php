<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BatController extends AbstractController
{
    /**
     * @Route("/bat", name="bat")
     */
    public function index()
    {
        return $this->render('bat/index.html.twig', [
            'controller_name' => 'BatController',
        ]);
    }
}
