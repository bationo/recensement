<?php

namespace App\Controller;

use App\Entity\Communal;
use App\Form\CommunalType;
use App\Repository\CommunalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/communal")
 */
class CommunalController extends AbstractController
{
    /**
     * @Route("/", name="communal_index", methods="GET")
     */
    public function index(CommunalRepository $communalRepository): Response
    {
        return $this->render('communal/index.html.twig', ['communals' => $communalRepository->findAll()]);
    }

    /**
     * @Route("/new/{id}", name="communal_new", methods="GET|POST")
     */
    public function new(Request $request, $id = null): Response
    {
        $communal = new Communal();
        $em = $this->getDoctrine()->getManager();

        if($id != null ){
            $communal = $em->getRepository(Communal::class)->find($id);
        }
        $form = $this->createForm(CommunalType::class, $communal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($communal);
            $em->flush();

            return $this->redirectToRoute('communal_index');
        }

        return $this->render('communal/new.html.twig', [
            'communal' => $communal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="communal_show", methods="GET")
     */
    public function show(Communal $communal): Response
    {
        return $this->render('communal/show.html.twig', ['communal' => $communal]);
    }

    /**
     * @Route("/{id}/edit", name="communal_edit", methods="GET|POST")
     */
    public function edit(Request $request, Communal $communal): Response
    {
        $form = $this->createForm(CommunalType::class, $communal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('communal_edit', ['id' => $communal->getId()]);
        }

        return $this->render('communal/edit.html.twig', [
            'communal' => $communal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="communal_delete", methods="DELETE")
     */
    public function delete(Request $request, Communal $communal): Response
    {
        if ($this->isCsrfTokenValid('delete'.$communal->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($communal);
            $em->flush();
        }

        return $this->redirectToRoute('communal_index');
    }
}
