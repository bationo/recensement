<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Form\ProduitType;
use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/produit")
 */
class ProduitController extends Controller
{
    /**
     * @Route("/", name="produit_index", methods="GET")
     */
    public function index(ProduitRepository $produitRepository): Response
    {
        return $this->render('produit/index.html.twig', ['produits' => $produitRepository->findAll()]);
    }

    /**
     * @Route("/new/{id}", name="produit_new", methods="GET|POST")
     */
    public function new(Request $request, $id = null ): Response
    {
        $produit = new Produit();
        $em = $this->getDoctrine()->getManager();
        if($id != null ){
            $produit = $em->getRepository(Produit::class)->find($id);
        }
       

        $form = $this->createForm(ProduitType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($produit);
            $em->flush();
            $request->getSession()->getFlashBag()->add('ajout', 'Opération réussie avec succès !');
            return $this->redirectToRoute('produit_show' , [ 'id' => $produit->getIdentifiant()  ] );
            
            
        }

        return $this->render('produit/new.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="produit_show", methods="GET")
     */
    public function show(Produit $produit): Response
    {
        return $this->render('produit/show.html.twig', ['produit' => $produit]);
    }

    /**
     * @Route("/{id}", name="produit_delete", methods="DELETE")
     */
    public function delete(Request $request, Produit $produit): Response
    {
        if ($this->isCsrfTokenValid('delete'.$produit->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($produit);
            $em->flush();
            $request->getSession()->getFlashBag()->add('ajout', 'L\'élément a bien été supprimé!'); 
        }

        return $this->redirectToRoute('produit_index');
    }
}
