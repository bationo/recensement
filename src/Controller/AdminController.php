<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\UserType;
use App\Form\PasswordEditType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
* @Route("/admin")
*/
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="admin")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
        ]);
    }

    /**
     * @Route("/logout", name="user_admin_logout")
     */
    public function logout()
    {

        return $this->render('user_admin/login.html.twig', [
        ]);
    }

    /**
     * @Route("/my-profil", name="user_admin_profil")
     */
    public function showCurrentUser(ObjectManager $em)
    {
        $entity = $this->getUser();
        return $this->render('user_admin/profil.html.twig', [
            'entity' => $entity
        ]);
    }

    /**
     * @Route("/edit-profil", name="user_admin_edit_profil")
     */
    public function editCurrentUser(Request $request,  ObjectManager $em)
    {
        $user = $this->getUser();
         
        $form = $this->createForm( UserType::class, $user , [ 'admin' => true , 'edit' => true ] );
        if ($form->handleRequest($request)->isSubmitted() &&  $form->isValid() ) {
            $em->persist($user);
            $em->flush();
            $request->getSession()->getFlashBag()->add('ajout', 'Opération réussie avec succès !'); 
            return $this->redirect($this->generateUrl('user_admin_edit_profil'));
        }

        return $this->render('user_admin/editUser.html.twig', [
            'form'   => $form->createView(),
            'user' => $user
        ]);
    }
}
